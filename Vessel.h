/*
This is a pseudocode of Prof. Thor I. Fossen's 6 degree of freedom vessl motion equations, for more information, please see
http://dl.kashti.ir/ENBOOKS/HMCH.pdf
This version assumes constant ship heading, wave period and wave height during the entire simulation,
which means there is no interpolation/extrapolation between different environment conditions.
There is no ocean current, no fluid memory effect in this version
*/

/*=================Terminology Definition===============*/
// eB: Body-fixed coordinate system, fixed on the moving rigid body, its 0-origin can have a distance from the center of mass of the rigid body
// eW: World-fixed coordinate system, fixed on a stationary point in 3D world. eW is a Newtonian frame.
// Both frames are right-handed frame with XY plane horizontal and Z axis pointing upwards at their upright position.
#define Real double
#define Gravity -9.81
class MyClass
{
public:

	/*====================Constant Parameters=====================*/
	// The mass of the rigid body ship. This scalar is independent from the selection of coordinate system or hydrodynamic behavior.  
	Real _mass;

	// The center of mass vector of the rigid body ship. This vector is w.r.t. eB, which makes it a constant.
	// Starting from the 0-origin of eB, ending on the center of mass.
	Real[3] _center_of_mass;
	
	// The moment of inertia of the rigid body ship. This value is dependent on the selection of coordinate system.
	// Here the value is define w.r.t the body-fixed coordinate system, whose 0-origin could be away from the center of mass
	Real[3][3] _moment_of_inertia;

	//// Hydrodynamic data
	// The 6-by-6 hydrodynamic matrices from SHIPX. These values are dependent on the selection of coordinate system.
	// Here the values are defined w.r.t the same coordinate system as _moment_of_inertia.
	Real[6][6] _added_mass;
	Real[6][6] _damping;
	Real[6][6] _restoring;

	/*===================Variables=================================*/
	Real[3] _velocity_pos; // Translational velocity of the 0-origin of ship's eB in eW.
	Real[3] _velocity_rot; // Rotational velocity of ship's eB in eW. NB: One object only has one uniform rotational velocity.
	Real[3] _position; // The position vector of 0-origin of eB in eW;
	Real[4] _rotation; // The quaternion rotation of eB w.r.t eW.

	/*===================Dynamic input=============================*/
	// External force and torque are defined w.r.t eB, meaning if there is a force/torque in eW, it needs to be transformed to eB before using it.
	Real[3] external_force;
	Real[3] external_torque;

	/*===================Methods===================================*/
	void Update(Real dt)
	{
		// RK4
		Real[13] k1, k2, k3, k4, y;
		y = Concatenate(_velocity_pos, _velocity_rot, _position, _rotation);
		k1 = GetDy(y, t);
		k2 = GetDy(y + 0.5 * dt * k1, t + 0.5 * dt);
		k3 = GetDy(y + 0.5 * dt * k2, t + 0.5 * dt);
		k4 = GetDy(y + dt * k3, t + dt);
		Real[13] y_new = y + (k1 + k2 * 2 + k3 * 2 + k4) * dt / 6;

		_velocity_pos = y_new[0 ~ 2];
		_velocity_rot = y_new[3 ~ 5];
		_position = y_new[6 ~ 8];
		_rotation = y_new[9 ~ 12];

		// Re-normalize quaternion
		Normalize(_rotation);
		
		Clear(external_force);
		Clear(external_torque);
		
	}

	// To accomodate RK4 integration, dy has to take in modifiable input during one step:
	Real[13] GetDy(Real[13] y, Real t)
	{
		Real[3] v_pos = y[0 ~2];
		Real[3] v_rot = y[3 ~ 5];
		Real[3] pos = y[6 ~ 8];
		Real[4] rot = y[9 ~12];

		/*====Step 1: Find acceleration in eB====*/
		Real[6] acc; // Translational & rotational acceleration in eB
		Real[6][6] m66 = GetInertiaMatrix6x6() + _added_mass6x6;
		Real[3] v_pos_b = QReverseRotate(rot, v_pos);
		Real[3] v_rot_b = QReverseRotate(rot, v_rot);
		Real[6] coriolis_force = GetCoriolisForce(m66, v_pos_b, v_rot_b);
		Real[3] gravity_force = QReverseRotate(rot, Concatnate(0, 0, _mass * Gravity));
		Real[3] gravity_torque = cross(_center_of_mass, gravity_force);
		Real[6] rhs = Concatnate(external_force, external_torque) + Concatnate(gravity_force, gravity_torque)- _damping * Concatnate(v_pos_b, v_rot_b) - _restoring * Concatnate(pos, Quat2Euler(rot)) - coriolis_force;
		acc = m66.solve(rhs);

		/*====Step 2: Transform to output variables========*/
		// Here dy is
		// 0 ~ 2: translational acceleration of the 0-origin of eB in eW
		// 3 ~ 5: rotational acceleration of eB in eW
		// 6 ~ 8: translational velocity of the 0-origin of eB in eW
		// 9 ~ 12: quaternion changing rate
		Real[13] return_vector;
		return_vector[0 ~ 2] = QRotate(rot, acc[0 ~ 2]) + cross(v_rot, v_pos);
		return_vector[3 ~ 5] = QRotate(rot, acc[3 ~ 5]);
		return_vector[6 ~ 8] = v_pos;
		return_vector[9 ~ 12] = 0.5 * QuatMultiply(Concatnate(0, v_rot), rot);
		return return_vector;
	};

	// Calculate the coriolis force
	Real[6] GetCoriolisForce(Real[6][6] m66, Real[3] v_pos_b, Real[3] v_rot_b)
	{
		Real[6] p = m66*Concatnate(v_pos_b,v_rot_b);
		Real[6] cori_force;
		cori_force[0 ~ 2] = cross(v_rot_b, p[0 ~ 2]);
		cori_force[3 ~ 5] = cross(v_pos_b, p[0 ~ 2]) + cross(v_rot_b, p[3~5]);
		return cori_force;
	}

	// Get 6-by-6 inertia matrix of the rigid body w.r.t eB
	Real[6][6] GetInertiaMatrix6x6()
	{
		Real[6][6] return_matrix;
		return_matrix[0 ~ 2][0 ~ 2] = _mass * GetIdentity3x3();
		return_matrix[0 ~ 2][3 ~ 5] = -_mass * GetSkewMatrix(_center_of_mass);
		return_matrix[3 ~ 5][0 ~ 2] = _mass * GetSkewMatrix(_center_of_mass);
		return_matrix[3 ~ 5][3 ~ 5] = _moment_of_inertia;
		return return_matrix;
	}

	// An interface function for external program to call and add force/torque.
	// You can call this function multiple times
	// force_vector is the literal force value you may already have.
	// acting_point is the position vector of the acting_point w.r.t the center of mass in eB
	// if_body_fixed is the identity of the force, whether it's defined in eB or eW 
	void AddBodyForce(Real[3] force_vector, Real[3] acting_point)
	{
		external_force += force_vector;
		external_torque += cross(acting_point, force_vector);
	}
	
	void AddWorldForce(Real[3] force_vector, Real[3] acting_point)
	{
		external_force += QReverseRotate(_rotation, force_vector);
		external_torque += cross(acting_point, QReverseRotate(_rotation, force_vector));
	}
	
	void AddBodyTorque(Real[3] torque_vector) // NB! torque doesn't need to have an acting_point
	{
		external_torque += torque_vector;
	}
	
	void AddWorldTorque(Real[3] torque_vector)
	{
		external_torque += QReverseRotate(_rotation, torque_vector);
	}


	/*===============Helper Functions=================*/
	// This function returns a skew-symmetric matrix that represents the cross product of a 3x1 vector
	// a X b = getSkewMatrix(a) * b;
	Real[3][3] GetSkewMatrix(Real[3] vector)
	{
		Real[3][3] return_matrix
			<< 0 << -vector[2] << vector[1] << endl
			<< vector[2] << 0 << -vector[0] << endl
			<< -vector[1] << vector[0] << 0 << endl;
		return return_matrix;
	}

	// Returns a 3x3 diagonal identity matrix
	Real[3][3] GetIdentity3x3()
	{
		Real[3][3] return_matrix
			<< 1 << 0 << 0 << endl
			<< 0 << 1 << 0 << endl
			<< 0 << 0 << 1 << endl;
	}

	// Multiply two quaternions q1 and q2
	Real[4] QuatMultiply(Real[4] q1, Real[4] q2)
	{
		Real[4] return_vector;
		Return_vector[0] = q1[0]*q2[0] - dot(q1[1~3], q2[1~3]);
		return_vector[1 ~ 3] = q1[0] * q2[1 ~ 3] + q2[0] * q1[1 ~ 3] + cross(q1[1 ~ 3], q2[1 ~ 3]);
		return return_vector;
	}
	// Rotate a 3x1 vector v by a quaternion q
	Real[3] QRotate(Real[4] q, Real[3] v)
	{
		Real[3] q_xyz = quat[1 ~ 3];
		Real[3] t = cross(q_xyz, v) * 2;
		Real[3] return_vector = v + q[0] * t + cross(q_xyz, t);
		return return_vector;
	}

	// Reverse rotate a 3x1 vector v by a quaternion q
	Real[3] QReverseRotate(Real[4] q, Real[3] v)
	{
		Real[3] q_xyz = - quat[1 ~3];
		Real[3] t = cross(q_xyz, v) * 2;
		Real[3] return_vector = v + q[0] * t + cross(q_xyz, t);
		return return_vector;
	}

	// Transform a quaternion q into intrinsic Euler angles in ZYX order
	Real[3] Quat2Euler(Real[4] q)
	{
		Real roll = atan2(2 * (q[0] * q[1] + q[2] * q[3]), 1 - 2 * (q[1] * q[1] + q[2] * q[2]));
		Real pitch = asin(2 * (q[0] * q[2] - q[3]*q[1]));
		Real yaw = atan2(2 * (q[0] * q[3] + q[1]*q[2]), 1 - 2 * (q[2]*q[2] + q[3]*q[3]));
		Real[3] return_vector << roll << pitch << yaw;
		return return_vector;
	}
};
