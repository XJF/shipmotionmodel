package no.ntnu.sfi.smm;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.IOException;

/**
 *
 * @author Lars Ivar Hatledal laht@ntnu.no.
 */
public class Main {

    public static void main(String[] args) throws IOException {
        ShipData data = ShipData.fromJson(new File("../../../ship_data.json"));
        ShipMotionModel smm = new ShipMotionModel(data);

        smm.addBodyForce(0, 0, 0, 0, 0, 0);
        for (int i = 0; i < 1000; i++) {
            smm.update(1d / 100);
            System.out.println(smm.getPosition());
        }

    }

}
