package no.ntnu.sfi.smm;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.ejml.simple.SimpleMatrix;

/**
 *
 * @author Lars Ivar Hatledal laht@ntnu.no.
 */
public class ShipMotionModel {

    private static final double GRAVITY = -9.81;

    private double t, mass;

    private final SimpleMatrix centerOfMass; //3x1
    private final SimpleMatrix momentOfInteria; //3x3

    private final SimpleMatrix addedMass6x6; //6x6
    private final SimpleMatrix damping; //6x6
    private final SimpleMatrix restoring; //6x6

    private SimpleMatrix linearVelocity = new SimpleMatrix(3, 1);
    private SimpleMatrix angularVelocity = new SimpleMatrix(3, 1);
    private SimpleMatrix position = new SimpleMatrix(3, 1);
    private SimpleMatrix rotation = new SimpleMatrix(4, 1);

    private final SimpleMatrix externalForce = new SimpleMatrix(3, 1);
    private final SimpleMatrix externalTorque = new SimpleMatrix(3, 1);

    public ShipMotionModel() {
        this.mass = 0;
        this.centerOfMass = new SimpleMatrix(3, 1);
        this.momentOfInteria = new SimpleMatrix(3, 3);
        this.addedMass6x6 = new SimpleMatrix(6, 6);
        this.damping = new SimpleMatrix(6, 6);
        this.restoring = new SimpleMatrix(6, 6);
    }

    public ShipMotionModel(ShipData data) {
        this.mass = data.getMass();
        this.centerOfMass = data.getCenterOfMass();
        this.momentOfInteria = data.getMomentOfInertia();
        this.addedMass6x6 = data.getAddedMass6x6();
        this.damping = data.getDamping();
        this.restoring = data.getRestoring();
    }

    public void configure(ShipData data) {
        this.mass = data.getMass();
        this.centerOfMass.set(data.getCenterOfMass());
        this.momentOfInteria.set(data.getMomentOfInertia());
        this.addedMass6x6.set(data.getAddedMass6x6());
        this.damping.set(data.getDamping());
        this.restoring.set(data.getRestoring());

    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public double getMass() {
        return mass;
    }

    public SimpleMatrix getPosition() {
        return position;
    }

    public SimpleMatrix getLinearVelocity() {
        return linearVelocity;
    }

    public SimpleMatrix getAngularVelocity() {
        return angularVelocity;
    }

    public SimpleMatrix getRotation() {
        return rotation;
    }

    public SimpleMatrix getExternalForce() {
        return externalForce;
    }

    public SimpleMatrix getExternalTorque() {
        return externalTorque;
    }

    public SimpleMatrix getMomentOfInteria() {
        return momentOfInteria;
    }

    public SimpleMatrix getCenterOfMass() {
        return centerOfMass;
    }

    public SimpleMatrix getAddedMass6x6() {
        return addedMass6x6;
    }

    public void addBodyForce(double fx, double fy, double fz, double x, double y, double z) {
        externalForce.set(0, externalForce.get(0) + fx);
        externalForce.set(1, externalForce.get(1) + fy);
        externalForce.set(2, externalForce.get(2) + fz);

        SimpleMatrix torque = Vec3.cross(Matrix.columnVector(x, y, z), Matrix.columnVector(fx, fy, fz));
        addBodyTorque(torque.get(0), torque.get(1), torque.get(2));
    }

    public void addWordForce(double fx, double fy, double fz, double x, double y, double z) {

        SimpleMatrix force_vector = Matrix.columnVector(fx, fy, fz);
        SimpleMatrix acting_vector = Matrix.columnVector(x, y, z);

        SimpleMatrix _force = Vec3.reverseRotate(force_vector, rotation);
        externalForce.set(0, externalForce.get(0) + _force.get(0));
        externalForce.set(1, externalForce.get(1) + _force.get(1));
        externalForce.set(2, externalForce.get(2) + _force.get(2));

        SimpleMatrix torque = Vec3.cross(acting_vector, Vec3.reverseRotate(force_vector, rotation));
        addBodyTorque(torque.get(0), torque.get(1), torque.get(2));

    }

    public void addBodyTorque(double tx, double ty, double tz) {
        externalTorque.set(0, externalTorque.get(0) + tx);
        externalTorque.set(1, externalTorque.get(1) + ty);
        externalTorque.set(2, externalTorque.get(2) + tz);
    }

    public void addWorldTorque(double tx, double ty, double tz) {
        SimpleMatrix torque = Vec3.reverseRotate(Matrix.columnVector(tx, ty, tz), rotation);
        addBodyTorque(torque.get(0), torque.get(1), torque.get(2));
    }

    public void update(double dt) {

        SimpleMatrix y = Matrix.concatenate(linearVelocity, angularVelocity, position, rotation);
        SimpleMatrix k1 = getDy(y, t);
        SimpleMatrix k2 = getDy(y.plus(k1.scale(0.5 * dt)), t + 0.5 * dt);
        SimpleMatrix k3 = getDy(y.plus(k2.scale(0.5 * dt)), t + 0.5 * dt);
        SimpleMatrix k4 = getDy(y.plus(k3.scale(dt)), t + dt);

        SimpleMatrix y_new = y.plus(k1.plus(k2.scale(2)).plus(k3.scale(2)).plus(k4).scale(dt / 6));
        linearVelocity = y_new.extractMatrix(0, 3, 0, 1);
        angularVelocity = y_new.extractMatrix(3, 6, 0, 1);
        position = y_new.extractMatrix(6, 9, 0, 1);
        rotation = y_new.extractMatrix(9, 13, 0, 1);

        Quat.normalize(rotation);

        externalForce.set(0);
        externalTorque.set(0);

        t += dt;

    }

    private SimpleMatrix getDy(SimpleMatrix y, double t) {

        SimpleMatrix vPos = y.extractMatrix(0, 3, 0, 1);
        SimpleMatrix vRot = y.extractMatrix(3, 6, 0, 1);
        SimpleMatrix pos = y.extractMatrix(6, 9, 0, 1);
        SimpleMatrix rot = y.extractMatrix(9, 13, 0, 1);

        SimpleMatrix m66 = getInertiaMatrix6x6().plus(addedMass6x6);
        SimpleMatrix v_pos_b = Vec3.reverseRotate(vPos, rot);
        SimpleMatrix v_rot_b = Vec3.reverseRotate(vRot, rot);
        SimpleMatrix coriolis_force = getCoriolisForce(m66, v_pos_b, v_rot_b);
        SimpleMatrix gravity_force = Vec3.reverseRotate(Matrix.columnVector(0, 0, mass * GRAVITY), rot);
        SimpleMatrix gravity_torque = Vec3.cross(centerOfMass, gravity_force);
        SimpleMatrix rhs = Matrix.concatenate(externalForce, externalTorque)
                .plus(Matrix.concatenate(gravity_force, gravity_torque))
                .minus(damping.mult(Matrix.concatenate(v_pos_b, v_rot_b)))
                .minus(restoring.mult(Matrix.concatenate(pos, Euler.setFromQuaternion(rot))))
                .minus(coriolis_force);

        SimpleMatrix acc = m66.solve(rhs);

        SimpleMatrix m1 = Vec3.rotate(acc.extractMatrix(0, 3, 0, 1), rot).plus(Vec3.cross(vRot, vPos));
        SimpleMatrix m2 = Vec3.rotate(acc.extractMatrix(3, 6, 0, 1), rot);
        SimpleMatrix m3 = vPos;
        SimpleMatrix m4 = Quat.mul(Matrix.columnVector(vRot.get(0), vRot.get(1), vRot.get(2), 0), rot).scale(0.5);

        return Matrix.concatenate(m1, m2, m3, m4);

    }

    private final static SimpleMatrix IDENTIY3X3 = SimpleMatrix.identity(3);

    private SimpleMatrix getInertiaMatrix6x6() {

        SimpleMatrix skewMatrix = getSkewMatrix(centerOfMass);

        SimpleMatrix m0 = IDENTIY3X3.scale(mass);
        SimpleMatrix m1 = skewMatrix.scale(-mass);
        SimpleMatrix m2 = skewMatrix.scale(mass);
        SimpleMatrix m3 = momentOfInteria;

        return new SimpleMatrix(new double[][]{
            {m0.get(0, 0), m0.get(0, 1), m0.get(0, 2), m1.get(0, 0), m1.get(0, 1), m1.get(0, 2)},
            {m0.get(1, 0), m0.get(1, 1), m0.get(1, 2), m1.get(1, 0), m1.get(1, 1), m1.get(1, 2)},
            {m0.get(2, 0), m0.get(2, 1), m0.get(2, 2), m1.get(2, 0), m1.get(2, 1), m1.get(2, 2)},
            {m2.get(0, 0), m2.get(0, 1), m2.get(0, 2), m3.get(0, 0), m3.get(0, 1), m3.get(0, 2)},
            {m2.get(1, 0), m2.get(1, 1), m2.get(1, 2), m3.get(1, 0), m3.get(1, 1), m3.get(1, 2)},
            {m2.get(2, 0), m2.get(2, 1), m2.get(2, 2), m3.get(2, 0), m3.get(2, 1), m3.get(2, 2)}
        });

    }

    private SimpleMatrix getSkewMatrix(SimpleMatrix vector) {
        return new SimpleMatrix(new double[][]{
            {0, -vector.get(2), vector.get(1)},
            {vector.get(2), 0, -vector.get(0)},
            {-vector.get(1), vector.get(0), 0}
        });
    }

    private static SimpleMatrix getCoriolisForce(SimpleMatrix m66, SimpleMatrix v_pos_b, SimpleMatrix v_rot_b) {

        SimpleMatrix p = m66.mult(Matrix.concatenate(v_pos_b, v_rot_b));

        SimpleMatrix p3_1 = p.extractMatrix(0, 3, 0, 1);
        SimpleMatrix p3_2 = p.extractMatrix(3, 6, 0, 1);

        SimpleMatrix a = Vec3.cross(v_rot_b, p3_1);
        SimpleMatrix b = Vec3.cross(v_pos_b, p3_1).plus(Vec3.cross(v_rot_b, p3_2));

        return Matrix.concatenate(a, b);
    }

    protected static class Vec3 {

        public static SimpleMatrix cross(SimpleMatrix d1, SimpleMatrix d2) {
            double d1_x = d1.get(0), d2_x = d2.get(0);
            double d1_y = d1.get(1), d2_y = d2.get(1);
            double d1_z = d1.get(2), d2_z = d2.get(2);

            double x = d1_y * d2_z - d1_z * d2_y;
            double y = d1_z * d2_x - d1_x * d2_z;
            double z = d1_x * d2_y - d1_y * d2_x;

            return Matrix.columnVector(x, y, z);

        }

        public static SimpleMatrix rotate(SimpleMatrix v, SimpleMatrix q) {
            return rotate(v, q, false);
        }

        public static SimpleMatrix reverseRotate(SimpleMatrix v, SimpleMatrix q) {
            return rotate(v, q, true);
        }

        public static SimpleMatrix rotate(SimpleMatrix v, SimpleMatrix q, boolean reverse) {
            double _x = v.get(0), _y = v.get(1), _z = v.get(2);
            double qx = q.get(0), qy = q.get(1), qz = q.get(2), qw = q.get(3);

            if (reverse) {
                qx *= -1;
                qy *= -1;
                qz *= -1;
            }

            // calculate quat * vector
            double ix = qw * _x + qy * _z - qz * _y;
            double iy = qw * _y + qz * _x - qx * _z;
            double iz = qw * _z + qx * _y - qy * _x;
            double iw = -qx * _x - qy * _y - qz * _z;

            // calculate result * inverse quat
            double x = ix * qw + iw * -qx + iy * -qz - iz * -qy;
            double y = iy * qw + iw * -qy + iz * -qx - ix * -qz;
            double z = iz * qw + iw * -qz + ix * -qy - iy * -qx;

            return Matrix.columnVector(x, y, z);
        }

    }

    protected static class Quat {

        public static SimpleMatrix mul(SimpleMatrix q1, SimpleMatrix q2) {

            double qax = q1.get(0), qay = q1.get(1), qaz = q1.get(2), qaw = q1.get(3);
            double qbx = q2.get(0), qby = q2.get(1), qbz = q2.get(2), qbw = q2.get(3);

            double x = qax * qbw + qaw * qbx + qay * qbz - qaz * qby;
            double y = qay * qbw + qaw * qby + qaz * qbx - qax * qbz;
            double z = qaz * qbw + qaw * qbz + qax * qby - qay * qbx;
            double w = qaw * qbw - qax * qbx - qay * qby - qaz * qbz;

            return Matrix.columnVector(x, y, z, w);
        }

        public static double length(SimpleMatrix q) {
            double x = q.get(0), y = q.get(1), z = q.get(2), w = q.get(3);
            return Math.sqrt(x * x + y * y + z * z + w * w);
        }

        public static void normalize(SimpleMatrix q) {
            double l = length(q);
            if (l == 0) {

                q.set(0, 0);
                q.set(1, 0);
                q.set(2, 0);
                q.set(3, 1);

            } else {

                l = 1d / l;

                q.set(0, q.get(0) * l);
                q.set(1, q.get(1) * l);
                q.set(2, q.get(2) * l);
                q.set(3, q.get(3) * l);

            }

        }

    }

    protected static class Matrix {

        public static SimpleMatrix concatenate(SimpleMatrix... ms) {

            SimpleMatrix m = ms[0];
            for (int i = 1; i < ms.length; i++) {
                m = m.combine(m.numRows(), 0, ms[i]);
            }
            return m;

        }

        public static SimpleMatrix columnVector(double... vals) {
            SimpleMatrix m = new SimpleMatrix(vals.length, 1);
            for (int i = 0; i < vals.length; i++) {
                m.set(i, 0, vals[i]);
            }
            return m;
        }

        /**
         * https://github.com/mrdoob/three.js/blob/dev/src/math/Matrix4.js
         * @param q quaternion
         * @return 3x3 rotation matrix
         */
        public static SimpleMatrix makeRotationFromQuaternion(SimpleMatrix q) {

            double x = q.get(0), y = q.get(1), z = q.get(2), w = q.get(3);
            double x2 = x + x, y2 = y + y, z2 = z + z;
            double xx = x * x2, xy = x * y2, xz = x * z2;
            double yy = y * y2, yz = y * z2, zz = z * z2;
            double wx = w * x2, wy = w * y2, wz = w * z2;

            return new SimpleMatrix(new double[][]{
                {1 - (yy + zz), xy - wz, xz + wy},
                {xy + wz, 1 - (xx + zz), yz - wx},
                {xz - wy, yz + wx, 1 - (xx + yy)}
            });

        }

    }

    protected static class Euler {

        enum EulerOrder {
            XYZ, YZX, ZXY, XZY, YXZ, ZYX
        }

        private static double clamp(double x, double min, double max) {
            return (x < min) ? min : ((x > max) ? max : x);
        }
        
        /**
         * https://github.com/mrdoob/three.js/blob/dev/src/math/Euler.js
         * @param q quaternion
         * @return euler representation (XYZ order)
         */
        static SimpleMatrix setFromQuaternion(SimpleMatrix q) {
            
            SimpleMatrix r = Matrix.makeRotationFromQuaternion(q);
            return setFromRotationMatrix(r, EulerOrder.XYZ);
            
        }

        /**
         * https://github.com/mrdoob/three.js/blob/dev/src/math/Euler.js
         *
         * @param m rotation matrix
         * @return euler representation (XYZ order)
         */
        static SimpleMatrix setFromRotationMatrix(SimpleMatrix m, EulerOrder order) {

            double x = 0, y = 0, z = 0;

            double m11 = m.get(0, 0), m12 = m.get(0, 1), m13 = m.get(0, 2);
            double m21 = m.get(1, 0), m22 = m.get(1, 1), m23 = m.get(1, 2);
            double m31 = m.get(2, 0), m32 = m.get(2, 1), m33 = m.get(2, 2);

            switch (order) {
                case XYZ: {
                    y = Math.asin(clamp(m13, - 1, 1));

                    if (Math.abs(m13) < 0.99999) {

                        x = Math.atan2(-m23, m33);
                        z = Math.atan2(-m12, m11);

                    } else {

                        x = Math.atan2(m32, m22);
                        z = 0;

                    }
                }
                break;
                case YXZ: {
                    x = Math.asin(-clamp(m23, - 1, 1));

                    if (Math.abs(m23) < 0.99999) {

                        y = Math.atan2(m13, m33);
                        z = Math.atan2(m21, m22);

                    } else {

                        y = Math.atan2(-m31, m11);
                        z = 0;

                    }
                }
                break;
                case ZXY: {
                    x = Math.asin(clamp(m32, - 1, 1));

                    if (Math.abs(m32) < 0.99999) {

                        y = Math.atan2(-m31, m33);
                        z = Math.atan2(-m12, m22);

                    } else {

                        y = 0;
                        z = Math.atan2(m21, m11);

                    }
                }
                break;
                case ZYX: {
                    y = Math.asin(-clamp(m31, - 1, 1));

                    if (Math.abs(m31) < 0.99999) {

                        x = Math.atan2(m32, m33);
                        z = Math.atan2(m21, m11);

                    } else {

                        x = 0;
                        z = Math.atan2(-m12, m22);

                    }
                }
                break;
                case YZX: {
                    z = Math.asin(clamp(m21, - 1, 1));

                    if (Math.abs(m21) < 0.99999) {

                        x = Math.atan2(-m23, m22);
                        y = Math.atan2(-m31, m11);

                    } else {

                        x = 0;
                        y = Math.atan2(m13, m33);

                    }
                }
                break;
                case XZY: {
                    z = Math.asin(-clamp(m12, - 1, 1));

                    if (Math.abs(m12) < 0.99999) {

                        x = Math.atan2(m32, m22);
                        y = Math.atan2(m13, m11);

                    } else {

                        x = Math.atan2(-m23, m33);
                        y = 0;

                    }
                }
            }

            return Matrix.columnVector(x, y, z);

        }

    }

}
