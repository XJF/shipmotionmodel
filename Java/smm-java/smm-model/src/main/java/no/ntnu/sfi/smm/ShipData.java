package no.ntnu.sfi.smm;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import org.apache.commons.io.FileUtils;
import org.ejml.simple.SimpleMatrix;

/**
 *
 * @author Lars Ivar Hatledal laht@ntnu.no.
 */
public class ShipData {

    private double mass;

    private double[] center_of_mass;

    private double[] moment_of_inertia;
    private double[] added_mass6x6;

    private double[] damping;
    private double[] restoring;

    public double getMass() {
        return mass;
    }

    public SimpleMatrix getCenterOfMass() {
        return toMatrix(center_of_mass, 3, 1);
    }

    public SimpleMatrix getMomentOfInertia() {
        return toMatrix(moment_of_inertia, 3, 3);
    }

    public SimpleMatrix getAddedMass6x6() {
        return toMatrix(added_mass6x6, 6, 6);
    }

    public SimpleMatrix getDamping() {
        return toMatrix(damping, 6, 6);
    }

    public SimpleMatrix getRestoring() {
        return toMatrix(restoring, 6, 6);
    }

    private static SimpleMatrix toMatrix(double[] flat, int rows, int cols) {

        if (rows * cols != flat.length) {
            throw new IllegalArgumentException();
        }

        int i = 0;
        SimpleMatrix m = new SimpleMatrix(rows, cols);
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                m.set(row, col, flat[i++]);
            }
        }
        return m;
    }

    public static ShipData fromJson(File file) throws IOException {
        return fromJson(FileUtils.readFileToString(file, Charset.forName("UTF-8")));
    }

    public static ShipData fromJson(String json) {
        return new Gson().fromJson(json, ShipData.class);
    }

    @Override
    public String toString() {
        return "ShipData{"
                + "mass=" + getMass()
                + "\ncenterOfMass=" + getCenterOfMass()
                + "\nmomentOfInertia=" + getMomentOfInertia()
                + "\naddedMass6x6=" + getAddedMass6x6()
                + "\ndamping=" + getDamping()
                + "\nrestoring=" + getRestoring()
                + '}';
    }

}
