/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.ntnu.sfi.smm;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.apache.commons.io.FileUtils;
import org.javafmi.proxy.Status;
import org.javafmi.wrapper.Simulation;

/**
 *
 * @author laht
 */
public class Main {

    public static void main(String[] args) throws IOException {

        Simulation fmu = new Simulation("../smm-export/ShipMotionModel.fmu");
        String config = FileUtils.readFileToString(new File("../../../ship_data.json"), Charset.forName("UTF-8"));
        fmu.write("ship_config").with(config);
        fmu.init(0);
        System.out.println("Mass=" + fmu.read("mass").asDouble());

        double t = 0, tEnd = 100;
        double dt = 1d / 100;
        while ((t += dt) < tEnd) {
            addBodyForce(fmu, 0, 0, fmu.read("mass").asDouble() * 9.81, 0, 0, 0);
            Status status = fmu.doStep(dt);
            if (status != Status.OK) {
                System.err.println("FMU update not OK: " + status);
                break;
            }
            System.out.println("Time=" + fmu.getCurrentTime() + "s, Position=" + Arrays.toString(readVector(fmu, "position", 3)));
        }
        fmu.terminate();

    }

    static double[] readVector(Simulation fmu, String name, int len) {
        double[] result = new double[len];
        for (int i = 0; i < len; i++) {
            result[i] = fmu.read(name + '[' + i + ']').asDouble();
        }
        return result;
    }

    static void addBodyForce(Simulation fmu, double fx, double fy, double fz, double x, double y, double z) {

        double[] force = new double[]{fx, fy, fz};
        for (int i = 0; i < force.length; i++) {
            fmu.write("body_force" + '[' + i + ']').with(force[i]);
        }
        double[] acting = new double[]{x, y, z};
        for (int i = 0; i < acting.length; i++) {
            fmu.write("body_force" + '[' + (i + 3) + ']').with(acting[i]);
        }

    }

}
