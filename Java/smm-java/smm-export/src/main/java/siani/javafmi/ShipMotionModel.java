/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siani.javafmi;

import no.ntnu.sfi.smm.ShipData;

/**
 *
 * @author Lars Ivar Hatledal laht@ntnu.no.
 */
public class ShipMotionModel extends org.javafmi.framework.FmiSimulation {

    private final static String MASS = "mass";
    private final static String BODY_FORCE = "body_force";
    private final static String BODY_TORQUE = "body_torque";
    private final static String WORLD_FORCE = "world_force";
    private final static String WORLD_TORQUE = "world_torque";
    private final static String POSITION = "position";
    private final static String ROTATION = "rotation";
    private final static String LINEAR_VELOCITY = "linear_velocity";
    private final static String ANGULAR_VELOCITY = "angular_velocity";
    private final static String MOMENT_OF_INERTIA = "moment_of_inertia";
    private final static String CENTER_OF_MASS = "center_of_mass";
    private final static String ADDED_MASS6x6 = "added_mass6x6";

    private final static String SHIP_CONFIGURATION = "ship_config";

    private final ShipMotionModelForExport model = new ShipMotionModelForExport();

    @Override
    public Model define() {

        Model model_ = model(getClass().getSimpleName())
                .canGetAndSetFMUstate(true)
                .author("Lars Ivar Hatledal")
                .description("Ship motion model")
                .add(variable(MASS).asReal().causality(Causality.parameter).variability(Variability.discrete))
                .description("The mass of the rigid body ship. This scalar is independent from the selection of coordinate system or hydrodynamic behavior")
                .add(variable(SHIP_CONFIGURATION).asString().causality(Causality.parameter).variability(Variability.discrete));

        for (int i = 0; i < 3; i++) {
            model_.add(variable(POSITION + '[' + i + ']').asReal().causality(Causality.calculatedParameter).variability(Variability.discrete))
                    .description("The position vector of 0-origin of eB in eW");
        }

        for (int i = 0; i < 3; i++) {
            model_.add(variable(LINEAR_VELOCITY + '[' + i + ']').asReal().causality(Causality.calculatedParameter).variability(Variability.discrete))
                    .description("Translational velocity of the 0-origin of ship's eB in eW");
        }

        for (int i = 0; i < 3; i++) {
            model_.add(variable(ANGULAR_VELOCITY + '[' + i + ']').asReal().causality(Causality.calculatedParameter).variability(Variability.discrete))
                    .description("Rotational velocity of ship's eB in eW. NB: One object only has one uniform rotational velocity");
        }

        for (int i = 0; i < 4; i++) {
            model_.add(variable(ROTATION + '[' + i + ']').asReal().causality(Causality.calculatedParameter).variability(Variability.discrete))
                    .description("The quaternion rotation of eB w.r.t eW");
        }

        for (int i = 0; i < 6; i++) {
            model_.add(variable(BODY_FORCE + '[' + i + ']').asReal().causality(Causality.input).variability(Variability.discrete));
        }

        for (int i = 0; i < 3; i++) {
            model_.add(variable(BODY_TORQUE + '[' + i + ']').asReal().causality(Causality.input).variability(Variability.discrete));
        }

        for (int i = 0; i < 6; i++) {
            model_.add(variable(WORLD_FORCE + '[' + i + ']').asReal().causality(Causality.input).variability(Variability.discrete));
        }

        for (int i = 0; i < 3; i++) {
            model_.add(variable(WORLD_TORQUE + '[' + i + ']').asReal().causality(Causality.input).variability(Variability.discrete));
        }

        for (int i = 0; i < 9; i++) {
            model_.add(variable(MOMENT_OF_INERTIA + '[' + i + ']').asReal().causality(Causality.parameter).variability(Variability.discrete))
                    .description("The moment of inertia of the rigid body ship. This value is dependent on the selection of coordinate system.\n"
                            + "Here the value is define w.r.t the body-fixed coordinate system, whose 0-origin could be away from the center of mass");
        }

        for (int i = 0; i < 9; i++) {
            model_.add(variable(CENTER_OF_MASS + '[' + i + ']').asReal().causality(Causality.parameter).variability(Variability.discrete))
                    .description("The center of mass vector of the rigid body ship. This vector is w.r.t. eB, which makes it a constant.\n"
                            + "Starting from the 0-origin of eB, ending on the center of mass");
        }

        for (int i = 0; i < 36; i++) {
            model_.add(variable(ADDED_MASS6x6 + '[' + i + ']').asReal().causality(Causality.parameter).variability(Variability.discrete))
                    .description("The 6-by-6 hydrodynamic matrices from SHIPX. These values are dependent on the selection of coordinate system.\n"
                            + "Here the values are defined w.r.t the same coordinate system as _moment_of_inertia.");
        }

        return model_;
    }

    @Override
    public Status init() {
        registerReal(MASS, () -> model.getMass(), val -> model.setMass(val));

        registerString(SHIP_CONFIGURATION, () -> "", val -> model.configure(ShipData.fromJson(val)));

        for (int i = 0; i < 3; i++) {
            final int j = i;
            registerReal(POSITION + '[' + i + ']', () -> model.getPosition().get(j));
        }

        for (int i = 0; i < 3; i++) {
            final int j = i;
            registerReal(LINEAR_VELOCITY + '[' + i + ']', () -> model.getLinearVelocity().get(j));
        }

        for (int i = 0; i < 3; i++) {
            final int j = i;
            registerReal(ANGULAR_VELOCITY + '[' + i + ']', () -> model.getAngularVelocity().get(j));
        }

        for (int i = 0; i < 6; i++) {
            final int j = i;
            registerReal(BODY_FORCE + '[' + i + ']', () -> model.getExternalForce().get(j), val -> model.addBodyForcePartial(j, val));
        }

        for (int i = 0; i < 3; i++) {
            final int j = i;
            registerReal(BODY_TORQUE + '[' + i + ']', () -> model.getExternalTorque().get(j), val -> model.addBodyTorquePartial(j, val));
        }

        for (int i = 0; i < 6; i++) {
            final int j = i;
            registerReal(WORLD_FORCE + '[' + i + ']', () -> model.getExternalForce().get(j), val -> model.addWorldForcePartial(j, val));
        }

        for (int i = 0; i < 3; i++) {
            final int j = i;
            registerReal(WORLD_TORQUE + '[' + i + ']', () -> model.getExternalTorque().get(j), val -> model.addWorldTorquePartial(j, val));
        }

        for (int i = 0; i < 3; i++) {
            final int j = i;
            registerReal(CENTER_OF_MASS + '[' + i + ']', () -> model.getCenterOfMass().get(j), val -> model.getCenterOfMass().set(j, val));
        }

        for (int i = 0; i < 4; i++) {
            final int j = i;
            registerReal(ROTATION + '[' + i + ']', () -> model.getRotation().get(j));
        }

        for (int i = 0; i < 9; i++) {
            final int j = i;
            registerReal(MOMENT_OF_INERTIA + '[' + i + ']', () -> model.getMomentOfInteria().get(j), var -> model.getMomentOfInteria().set(j, var));
        }

        for (int i = 0; i < 36; i++) {
            final int j = i;
            registerReal(ADDED_MASS6x6 + '[' + i + ']', () -> model.getAddedMass6x6().get(j), var -> model.getAddedMass6x6().set(j, var));
        }

        return Status.OK;
    }

    @Override
    public Status doStep(double dt) {

        try {
            model.update(dt);
        } catch (org.ejml.factory.SingularMatrixException ex) {
            logger().error(ex.getMessage());
            return Status.FATAL;
        }

        return Status.OK;
    }

    @Override
    public Status reset() {
        return Status.OK;
    }

    @Override
    public Status terminate() {
        return Status.OK;
    }

}
