/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siani.javafmi;

import org.ejml.simple.SimpleMatrix;

/**
 *
 * @author Lars Ivar Hatledal laht@ntnu.no.
 */
public class ShipMotionModelForExport extends no.ntnu.sfi.smm.ShipMotionModel {

    private final TmpForce tmpBodyForce = new TmpForce();
    private final TmpForce tmpWorldForce = new TmpForce();

    private final TmpTorqe tmpBodyTorque = new TmpTorqe();
    private final TmpTorqe tmpWorldTorque = new TmpTorqe();

    public ShipMotionModelForExport() {
    }

    public void addBodyForcePartial(int i, double d) {
        tmpBodyForce.add(i, d);
    }

    public void addWorldForcePartial(int i, double d) {
        tmpWorldForce.add(i, d);
    }

    private void addBodyForce(SimpleMatrix m) {
        addBodyForce(m.get(0), m.get(1), m.get(2), m.get(3), m.get(4), m.get(5));
    }

    private void addWorldForce(SimpleMatrix m) {
        addWordForce(m.get(0), m.get(1), m.get(2), m.get(3), m.get(4), m.get(5));
    }

    public void addBodyTorquePartial(int i, double d) {
        tmpBodyTorque.add(i, d);
    }

    private void addWorldTorque(SimpleMatrix m) {
        addWorldTorque(m.get(0), m.get(1), m.get(2));
    }

    public void addWorldTorquePartial(int i, double d) {
        tmpWorldTorque.add(i, d);
    }

    private void addBodyTorque(SimpleMatrix m) {
        addBodyTorque(m.get(0), m.get(1), m.get(2));
    }

    private void checkTmp() {
        //force
        SimpleMatrix bodyForce = tmpBodyForce.onUpdate();
        if (bodyForce != null) {
            addBodyForce(bodyForce);
        }
        SimpleMatrix worldForce = tmpWorldForce.onUpdate();
        if (worldForce != null) {
            addWorldForce(worldForce);
        }
        //torque
        SimpleMatrix bodyTorque = tmpBodyTorque.onUpdate();
        if (bodyTorque != null) {
            addBodyTorque(bodyTorque);
        }
        SimpleMatrix worldTorque = tmpWorldTorque.onUpdate();
        if (worldTorque != null) {
            addWorldTorque(worldTorque);
        }
    }

    @Override
    public void update(double dt) {
        checkTmp();
        super.update(dt);
    }

    static class TmpTorqe {

        double pointer = 0;
        double[] torque = new double[3];

        public void add(int i, double d) {

            if (i < 0) {
                throw new IllegalArgumentException();
            }
            if (i > 2) {
                throw new IllegalArgumentException();
            }

            torque[i] = d;

            pointer++;
        }

        boolean isComplete() {
            return pointer == 3;
        }

        void clear() {
            pointer = 0;
            torque[0] = 0;
            torque[1] = 0;
            torque[2] = 0;
        }

        SimpleMatrix onUpdate() {
            SimpleMatrix result = null;
            if (isComplete()) {
                result = no.ntnu.sfi.smm.ShipMotionModel.Matrix.columnVector(torque[0], torque[1], torque[2]);
            }
            clear();
            return result;

        }
    }

    static class TmpForce {

        double pointer = 0;
        double[] force = new double[3];
        double[] acting = new double[3];

        public void add(int i, double d) {

            if (i < 0) {
                throw new IllegalArgumentException();
            }

            if (i < 3) {
                force[i] = d;
            } else if (i < 6) {
                acting[i - 3] = d;
            } else {
                throw new IllegalArgumentException();
            }
            pointer++;
        }

        boolean isComplete() {
            return pointer == 6;
        }

        void clear() {
            pointer = 0;
            force[0] = 0;
            force[1] = 0;
            force[2] = 0;
            acting[0] = 0;
            acting[1] = 0;
            acting[2] = 0;
        }

        SimpleMatrix onUpdate() {
            SimpleMatrix result = null;
            if (isComplete()) {
                result = no.ntnu.sfi.smm.ShipMotionModel.Matrix.columnVector(force[0], force[1], force[2], acting[0], acting[1], acting[2]);
            }
            clear();
            return result;

        }

    }

}
